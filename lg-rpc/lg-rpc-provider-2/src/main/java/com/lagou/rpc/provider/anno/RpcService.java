package com.lagou.rpc.provider.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Desc    对外暴露服务接口
 * @Author Matures
 * @CreateTime 2021/4/10 22:08
 **/
@Target(ElementType.TYPE) // 用于接口和类上
@Retention(RetentionPolicy.RUNTIME) // 在运行是可以获取到
public @interface RpcService {

}
