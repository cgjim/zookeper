package com.lagou.rpc.consumer.proxy;

import com.alibaba.fastjson.JSON;
import com.lagou.rpc.common.RpcRequest;
import com.lagou.rpc.common.RpcResponse;
import com.lagou.rpc.consumer.client.RpcClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.UUID;
/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/11 10:27
 **/
public class RpcClientProxy {

    private int port;

    public RpcClientProxy(int port) {
        this.port = port;
    }

    private RpcClient rpcClient;

    public RpcClientProxy(RpcClient rpcClient){
        this.rpcClient = rpcClient;
    }

    public Object createProxy(Class serviceCLass) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{serviceCLass},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        // 封装RpcRequest对象
                        RpcRequest rpcRequest = new RpcRequest();
                        rpcRequest.setRequestId(UUID.randomUUID().toString());
                        rpcRequest.setClassName(serviceCLass.getName());
                        rpcRequest.setMethodName(method.getName());
                        rpcRequest.setParameterTypes(method.getParameterTypes());
                        rpcRequest.setParameters(args);
                        // 创建RpcClient
                        RpcClient rpcClient = new RpcClient("127.0.0.1", port);
                        try {
                            Object responseMsg = rpcClient.send(JSON.toJSONString(rpcRequest));
                            RpcResponse response = JSON.parseObject(responseMsg.toString(), RpcResponse.class);
                            if (response.getError() != null) {
                            throw new RuntimeException(response.getError());
                        }
                            return JSON.parseObject(response.getResult().toString(), method.getReturnType());
                        } catch (Exception e) {
                            throw e;
                        } finally {
                            rpcClient.close();
                        }
                    }
                });
    }



    public Object createZKProxy(Class serviceCLass) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{serviceCLass},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        // 封装RpcRequest对象
                        RpcRequest rpcRequest = new RpcRequest();
                        rpcRequest.setRequestId(UUID.randomUUID().toString());
                        rpcRequest.setClassName(serviceCLass.getName());
                        rpcRequest.setMethodName(method.getName());
                        rpcRequest.setParameterTypes(method.getParameterTypes());
                        rpcRequest.setParameters(args);
                        try {
                            Object responseMsg = rpcClient.send(JSON.toJSONString(rpcRequest));
                            RpcResponse response = JSON.parseObject(responseMsg.toString(), RpcResponse.class);
                            if (response.getError() != null) {
                                throw new RuntimeException(response.getError());
                            }
                            return JSON.parseObject(response.getResult().toString(), method.getReturnType());
                        } catch (Exception e) {
                            throw e;
                        }
                    }
                });
    }

}
